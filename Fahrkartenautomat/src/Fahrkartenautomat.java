﻿import java.awt.print.Printable;
import java.sql.SQLNonTransientConnectionException;
import java.util.Scanner;

import javax.management.loading.PrivateClassLoader;
//new
//Leon Langrock-Weise
class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double fahrkartenPreis;
		double eingezahlterGesamtbetrag;
		double zuZahlenderBetrag;
		double rückgabebetrag;
		int anzahlFahrkarten;
		double gesamtPreis;
		char weiter = 'j';
		char schluss = 'n';
	
		do {
			fahrkartenPreis = fahrkartenWählen(tastatur);
			rückgabebetrag = fahrkartenBezahlen(tastatur, fahrkartenPreis);
			fahrkartenAusgeben(tastatur);
			rückgeldAusgebe(rückgabebetrag, fahrkartenPreis);
			System.out.println("Wollen Sie mehr Fahrkarten kaufen? [j/n]: ");
		}while (tastatur.next().equals("j"));
			System.out.println("--Fahrkartenautomat wurde heruntergefahren--");
	}

	
	
	
	
	private static double fahrkartenWählen(Scanner tastatur) {
		 int anzahlTickets;
		 double ticketPreis = 0.0;

		 String[] Fahrscheine = { "1 Einzelfahrschein Berlin AB", "2 Einzelfahrschein Berlin BC",
		 "3 Einzelfahrschein Berlin ABC", "4 Kurzstrecke", "5 Tageskarte Berlin AB", "6 Tageskarte Berlin BC", 
		 "7 Tageskarte Berlin ABC", "8 Kleingruppen-Tageskarte Berlin AB", "9 Kleingruppen-Tageskarte Berlin BC",
		 "10 Kleingruppen-Tageskarte Berlin ABC" };
		 double[] preis = { 2.9, 3.3, 3.6, 1.9, 8.6, 9, 9.6, 23.5, 24.3, 24.9 };

		 System.out.println("Fahrkartenbestellvorgang:");
		 System.out.println("==============================\n");
		 System.out.println("Waehlen Sie ihre Wunschfahrkarte aus:");

		 for (int i = 0; i < Fahrscheine.length; i++) {
		 System.out.println(Fahrscheine[i]);
		 }
		 ticketPreis = preis[tastatur.nextInt() - 1]; 
		 System.out.print("Anzahl der Tickets: ");
		 anzahlTickets = tastatur.nextInt();

		 if (anzahlTickets < 1 || anzahlTickets > 10) {
		 anzahlTickets = 1;
		 }

		 return ticketPreis * anzahlTickets;
		 }
	
	private static double fahrkartenBezahlen(Scanner tastatur, double gesamtPreis ) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < gesamtPreis) {
			System.out.printf("Noch zu zahlen: %.2f EURO\n", (gesamtPreis - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag;
	}
	
	
	private static void fahrkartenAusgeben(Scanner tastatur) {
		
	        System.out.println("\nFahrschein wird ausgegeben");
	        for (int i = 0; i < 8; i++) {
	            System.out.print("=");
	            try {
	                Thread.sleep(250);
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            }
	        }
	        System.out.println("\n\n");
	}

	
	private static void rückgeldAusgebe(double rückgabebetrag, double gesamtPreis) {
		double rückgabe;
		
		rückgabe = rückgabebetrag - gesamtPreis;
		if (rückgabe > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", rückgabe);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabe >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabe -= 2.0;
			}
			while (rückgabe >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabe -= 1.0;
			}
			while (rückgabe >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("0.50 EURO");
				rückgabe -= 0.5;
			}
			while (rückgabe >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("0.20 EURO");
				rückgabe -= 0.2;
			}
			while (rückgabe >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("0.10 EURO");
				rückgabe -= 0.1;
			}
			while (rückgabe >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("0.05 EURO");
				rückgabe -= 0.05;
			}
		}
		
			System.out.println("\nVergessen Sie nicht, die Fahrscheine\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.\n");
	}
}